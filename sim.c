#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

typedef struct {
	int cas;	//casovy usek 
	int body;	//deliace body
	int trajekt;	//pocet trajektorii
	float delta_t;	//casovy krok
}PARAMETER;

float** nastav(PARAMETER b) 
{
	int i;
	float **temp;
	temp = (float**) malloc(b.trajekt * sizeof(float*));
	for(i = 0; i < b.trajekt; i++) {
		temp[i] = (float*) malloc((b.body + 1) * sizeof(float));
	}
	return temp;
}

float normalne_rozdelenie(float delta_t)
{
	float rovnomer_rozdelenie1, rovnomer_rozdelenie2, nahodne_cislo;
	
	rovnomer_rozdelenie1 = (double)rand()/RAND_MAX;
	rovnomer_rozdelenie2 = (double)rand()/RAND_MAX;
	nahodne_cislo = sqrt(-2*log(rovnomer_rozdelenie1))*cos(2*M_PI*rovnomer_rozdelenie2);
	
	return nahodne_cislo * delta_t;	//nahodne cislo z N(0, delta_t)
}

float pravdepod(int vyhovujuce, PARAMETER b)
{
	printf("Pravdepodobnost je: %lf\n", (vyhovujuce / (float)b.trajekt));
	return (vyhovujuce / (float)b.trajekt);
}

void vytvor_trajektorie(float **temp, PARAMETER b)
{
	int i,j, pom;
	float inkrement;
	pom = 0;

	for(i = 0; i < b.trajekt; i++) {
		for (j = 0; j < b.body; j++) {
			if (j != 0 ){
				pom++;
				inkrement = normalne_rozdelenie(b.delta_t);
				temp[i][j] = temp[i][j - 1] + inkrement;
			}
			else{ 
				temp[i][j] = 0;
			} 
		}
	}
}

int subor(float **temp, PARAMETER b)
{
	FILE *fileTr, *fileVk;
	int i = 0, j = 0, vyhovuj = 0;
	
	fileTr = fopen("trajektorie10000.dat", "w");
	fileVk = fopen("vektory10000.dat","w");
	for(i = 0; i < b.trajekt; i++ ) {
		if ((temp[i][1] > 0) && (temp[i][2] <= (2 * temp[i][1]))) {
			for(j = 0; j < b.body; j++) {
				fprintf(fileTr, "%lf %lf\n", j * b.delta_t, temp[i][j]);
			}
			fprintf(fileVk, "%lf %lf\n", temp[i][1], temp[i][2]);
			fprintf(fileTr, "\n");
			vyhovuj++;
		}
	}
	fprintf(fileTr, "\n");
	fprintf(fileVk, "\n\n");
	for(i = 0; i < b.trajekt; i++ ) {
		if ((temp[i][1] < 0) || (temp[i][2] >= (2 * temp[i][1]))) {
			for(j = 0; j < b.body; j++) {
				fprintf(fileTr, "%lf %lf\n", j * b.delta_t, temp[i][j]);
			}
			fprintf(fileVk, "%lf %lf\n", temp[i][1], temp[i][2]);
			fprintf(fileTr, "\n");
		}
	}
	fclose(fileTr);
	fclose(fileVk);
	
	return vyhovuj;
}

void dealokacia(float **temp, PARAMETER b)
{
	int i;
	for(i = 0; i < b.trajekt; i++) 
	free(temp[i]);
	free(temp);
}

int main()
{
	int cas, poc_deliac_bodov, poc_trajekt, vyhovuj;
	float **nahodna_prem, PP;
	PARAMETER b;
	
	printf("\nZadajte pocet trajektorii: ");
	scanf("%d",&b.trajekt);
	printf("\nZadajte pocet deliacich bodov: ");
	scanf("%d",&b.body);
	printf("\nZadajte dlzku casoveho useku: ");
	scanf("%d",&b.cas);
	b.delta_t = b.cas / (float)b.body;
	
	nahodna_prem = nastav(b);
	vytvor_trajektorie(nahodna_prem, b);
	vyhovuj = subor(nahodna_prem, b);
	PP = pravdepod(vyhovuj, b);
	dealokacia(nahodna_prem, b);
}
